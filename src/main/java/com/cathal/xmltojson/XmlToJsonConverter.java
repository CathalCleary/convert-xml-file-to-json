/**
 * @author Cathal
 */
package com.cathal.xmlToJson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.json.JSONObject;
import org.json.XML;

import com.cathal.fileWriter.MyFileWriter;

public class XmlToJsonConverter {
	private static final int INDENT_FACTOR = 4;
	private static final String JSON_FILE_NAME = "jsonConverterBooksJson.json";
	
	private static String readXml(){
		BufferedReader bufferedReader = null;
		String xmlString = "";
		try {
			bufferedReader = new BufferedReader(new FileReader("src/main/resources/Books.xml"));
			String currentParsingLine = "";
			System.out.println("Reading xml...");
			while((currentParsingLine  = bufferedReader.readLine()) != null){
				xmlString += currentParsingLine;
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
		return xmlString;
	}
	
	public static void convertXmlToJson() throws IOException {
		System.out.println("Converting xml to json...");
		final JSONObject xmlJSONObj = XML.toJSONObject(readXml());
        final String jsonPrettyPrintString = xmlJSONObj.toString(INDENT_FACTOR);
        MyFileWriter.writeFile(jsonPrettyPrintString, JSON_FILE_NAME);
	}

	public static void main(String[] args) throws IOException {
		convertXmlToJson();
		System.out.println("Json written to target/json-files/jsonConverterBooks.Json");
	}

}
