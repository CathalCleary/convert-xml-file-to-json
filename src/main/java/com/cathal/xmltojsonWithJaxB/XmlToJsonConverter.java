/**
 * @author Cathal
 */
package com.cathal.xmltojsonWithJaxB;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.persistence.jaxb.MarshallerProperties;

import com.cathal.fileWriter.MyFileWriter;

public class XmlToJsonConverter {
	
	private static String UNMARSALLED_FILE = "unmarshalledFile.txt";
	private static String PATH_TO_JAXB_JSON = "target/json-files/JaxBBooks.json";
	private static String CSV_FILE = "Books.csv";
	
	private static void writeUnmarshalledDataToFile(Catalog myCatalog){
		System.out.println("Writing unmarshalled data to file");
		StringBuilder stringBuilder = new StringBuilder();
		
		for(Book book: myCatalog.getBook()){
			stringBuilder.append("Title: " + book.getTitle() + "\n"
			+ " " + "Description: " + book.getDescription() + "\n"
			+ " " + "Genre: " + book.getGenre() + "\n"
			+ " " +  "Price: " + book.getPrice() + "\n"
			+ " " + "Publish Date " + book.getPublishDate() + "\n"
			+"\n\n");
		}
		
		MyFileWriter.writeFile(stringBuilder.toString(), UNMARSALLED_FILE);
		
		System.out.println("Unmarshalled data written to target/json-files/unmarshalledFile.txt");
	}
	
	public static void writeToCsvFile(Catalog myCatalog){
		System.out.println("Generating csv file...");
		final String DELIMITER = ",";
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append("Title"+DELIMITER);
		stringBuffer.append("Genre" + DELIMITER);
		stringBuffer.append("Price" + DELIMITER);
		stringBuffer.append("Publish Date" + DELIMITER);
		stringBuffer.append("Description" + DELIMITER);
		
		for(Book book: myCatalog.getBook()){
			stringBuffer.append("\n");
			stringBuffer.append(book.getTitle() + DELIMITER);
			stringBuffer.append(book.getGenre() + DELIMITER);
			stringBuffer.append(book.getPrice() + DELIMITER);
			stringBuffer.append(book.getPublishDate() + DELIMITER);
			stringBuffer.append(book.getDescription() + DELIMITER);
		}
		MyFileWriter.writeFile(stringBuffer.toString(), CSV_FILE);
	}
	
	public static void convertXmlToJson() throws JAXBException{
		System.out.println("Unmarsalling xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Catalog myCatalog = (Catalog) jaxbUnmarshaller.unmarshal(new File("src/main/resources/Books.xml"));
		writeUnmarshalledDataToFile(myCatalog);
		writeToCsvFile(myCatalog);
		System.out.println("marshalling to Json");
		Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(myCatalog, new File(PATH_TO_JAXB_JSON));
        System.out.println("marshalled to: target/json-files/Books.json");
		
	}
	
	public static void main(String[] args) throws JAXBException {
		convertXmlToJson();
	}

}
