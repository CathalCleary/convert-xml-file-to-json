/**
 * @author Cathal
 */

package com.cathal.xmltojsonWithJaxB;

import java.util.List;
import javax.xml.bind.annotation.*;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="catalog")
@XmlAccessorType (XmlAccessType.FIELD)
public class Catalog {
	private List<Book> book;
	
	public List<Book> getBook(){
		return book;
	}

	public void setBook(List<Book> book) {
		this.book = book;
	}
	
}
