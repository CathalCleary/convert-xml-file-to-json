/**
 * @author Cathal
 */

package com.cathal.xmltojsonWithJaxB;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "book")
@XmlAccessorType (XmlAccessType.FIELD)
public class Book {
	private String title;
	private String genre;
	private double price;
	@XmlElement(name = "publish_date")
	private String publishDate;
	private String description;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publish_Date) {
		this.publishDate = publish_Date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
