/**
 * @author Cathal
 */
package com.cathal.fileWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public final class MyFileWriter {
	private MyFileWriter(){
	}
	
	public static void writeFile(String content, String fileName) {
		File file = new File("target/json-files/" + fileName);
		FileWriter fileWriter;
		PrintWriter printWriter = null;
		System.out.println("Writing json to file");
		try {
			fileWriter = new FileWriter(file);
			printWriter = new PrintWriter(fileWriter);
			printWriter.write(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			printWriter.close();
		}
	}
	
}
